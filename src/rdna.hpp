#include <iostream>
#include <string>
#include <random>

using namespace std;

string randDNA(int seed, string bases, int n)
{
	int min = 0;
	int max = bases.size() -1;
	int index = 0;
	string dna = "";
	
	mt19937 engine(seed);
	uniform_int_distribution<> unifrm(min, max);
	
	for (int i = 0; i < n; i++)
	{
		index = unifrm(engine);
		dna += bases[index];
	}
	
	return dna;

}
